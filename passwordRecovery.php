<?php
require_once 'headhome.php';
require_once 'models/user.php';
require_once 'controllers/changingPassword-Controller.php';
?>
<div class="imgBgk row">
    <h1 class="center-align marginTop">Modifie ton mot de passe</h1>
    <form action="" method="post" class="marginTop row">
        <div class="marginTopMin col s6 offset-s3 input-field">
            <input type="email" name="mailUser" id="mailUser" class="validate" required />
            <label for="mail" class="black-text">Adresse mail</label>
        </div>
        <div class="marginTop col s6 offset-s3 input-field">
            <input type="password" name="password" id="password" class="validate" required />
            <label for="password" class="black-text">Nouveau mot de passe</label>
        </div>
        <?php foreach ($newPasswordArray as $message) { ?>
            <p class="col s6 offset-s3 center-align marginTop"><?= $message ?></p>
        <?php } ?>
        <!--Permet d'envoyer les modification du mot de passe-->
        <div class="col s2 offset-s5 center marginTop">
            <input type="submit" class=" btn amber waves-effect waves-orange" value="Envoyer" id="changingSubmitPassword" name="changingSubmitPassword" />
        </div>
    </form>
</div>
<?php require_once 'footer.php';